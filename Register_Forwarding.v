module Register_Forwarding (input [3:0] ID_EX_Op1, ID_EX_Op2, EX_MEM_Op1, MEM_WB_Op1, input [1:0] EX_MEM_RegWrite, MEM_WB_RegWrite, output reg [1:0] ForwardA, ForwardB);

//always @(*)
//	$monitor($time, "  A  %b  B  %b",ForwardA, ForwardB);
	

always @(*)
begin 
	if((EX_MEM_RegWrite == 2'b01 || EX_MEM_RegWrite == 2'b11) && (MEM_WB_RegWrite == 2'b01 || MEM_WB_RegWrite == 2'b11))
		if(ID_EX_Op1 == ID_EX_Op2)	
			if((ID_EX_Op1 == EX_MEM_Op1) && (ID_EX_Op1 != MEM_WB_Op1))
				begin
					ForwardA = 2'b01; 
					ForwardB = 2'b01;
				end
			else if((ID_EX_Op1 != EX_MEM_Op1) && (ID_EX_Op1 == MEM_WB_Op1))
				begin
					ForwardA = 2'b10; 
					ForwardB = 2'b10;
				end
			else if((ID_EX_Op1 == EX_MEM_Op1) && (ID_EX_Op1 == MEM_WB_Op1))
				begin
					ForwardA = 2'b01; 
					ForwardB = 2'b01;
				end
			else
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b00;
				end
		else if(ID_EX_Op1 != ID_EX_Op2)	
			if((ID_EX_Op1 == EX_MEM_Op1) && (ID_EX_Op2 == MEM_WB_Op1))
				begin
					ForwardA = 2'b01; 
					ForwardB = 2'b10;
				end
			else if((ID_EX_Op1 == MEM_WB_Op1) && (ID_EX_Op2 == EX_MEM_Op1)) 
				begin
					ForwardA = 2'b10; 
					ForwardB = 2'b01;
				end
			else if((ID_EX_Op1 == EX_MEM_Op1) && (ID_EX_Op1 == MEM_WB_Op1))
				begin
					ForwardA = 2'b01; 
					ForwardB = 2'b00;
				end
			else if((ID_EX_Op1 == EX_MEM_Op1) && (ID_EX_Op1 != MEM_WB_Op1) && (ID_EX_Op2 != MEM_WB_Op1))
				begin
					ForwardA = 2'b01; 
					ForwardB = 2'b00;
				end	
			else if((ID_EX_Op1 != EX_MEM_Op1) && (ID_EX_Op1 == MEM_WB_Op1) && (ID_EX_Op2 != EX_MEM_Op1))
				begin
					ForwardA = 2'b10; 
					ForwardB = 2'b00;
				end	
			else if((ID_EX_Op2 == EX_MEM_Op1) && (ID_EX_Op2 == MEM_WB_Op1))
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b01;
				end	
			else if((ID_EX_Op2 == EX_MEM_Op1) && (ID_EX_Op2 != MEM_WB_Op1) && (ID_EX_Op1 != MEM_WB_Op1))
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b01;
				end	
			else if((ID_EX_Op2 != EX_MEM_Op1) && (ID_EX_Op2 == MEM_WB_Op1) && (ID_EX_Op1 != EX_MEM_Op1))
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b10;
				end	
			else 
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b00;
				end

	if((EX_MEM_RegWrite == 2'b00 || EX_MEM_RegWrite == 2'b10) && (MEM_WB_RegWrite == 2'b01 || MEM_WB_RegWrite == 2'b11))
		if(ID_EX_Op1 == ID_EX_Op2)
			if(ID_EX_Op1 == MEM_WB_Op1)
				begin
					ForwardA = 2'b10; 
					ForwardB = 2'b10;
				end
			else
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b00;
				end
		else if(ID_EX_Op1 != ID_EX_Op2)
			if(ID_EX_Op1 == MEM_WB_Op1)
				begin
					ForwardA = 2'b10; 
					ForwardB = 2'b00;
				end	
			else if(ID_EX_Op2 == MEM_WB_Op1)
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b10;
				end
			else 
				begin
					ForwardA = 2'b00; 
					ForwardB = 2'b00;
				end		
				
	if((EX_MEM_RegWrite == 2'b01 || EX_MEM_RegWrite == 2'b11) && (MEM_WB_RegWrite == 2'b00 || MEM_WB_RegWrite == 2'b10))	
			if(ID_EX_Op1 == ID_EX_Op2)
				if(ID_EX_Op1 == EX_MEM_Op1)
					begin
						ForwardA = 2'b01; 
						ForwardB = 2'b01;
					end
				else 
					begin
						ForwardA = 2'b00; 
						ForwardB = 2'b00;
					end
			else if(ID_EX_Op1 != ID_EX_Op2)
				if(ID_EX_Op1 == EX_MEM_Op1)
					begin
						ForwardA = 2'b01; 
						ForwardB = 2'b00;
					end	
				else if(ID_EX_Op2 == EX_MEM_Op1)
					begin
						ForwardA = 2'b00; 
						ForwardB = 2'b01;
					end
				else 
					begin
						ForwardA = 2'b00; 
						ForwardB = 2'b00;
					end

	if((EX_MEM_RegWrite == 2'b00 || EX_MEM_RegWrite == 2'b10) && (MEM_WB_RegWrite == 2'b00 || MEM_WB_RegWrite == 2'b10))	
		begin
			ForwardA = 2'b00; 
			ForwardB = 2'b00;
		end
end
endmodule



/*
ForwardA = 00 ID/EX The first ALU operand comes from the register file.
ForwardA = 01 EX/MEM The first ALU operand is forwarded from the prior ALU result.
ForwardA = 10 MEM/WB The first ALU operand is forwarded from data memory or an earlier
ALU result.
ForwardB = 00 ID/EX The second ALU operand comes from the register file.
ForwardB = 01 EX/MEM The second ALU operand is forwarded from the prior ALU result.
ForwardB = 10 MEM/WB The second ALU operand is forwarded from data memory or an
earlier ALU result.
*/