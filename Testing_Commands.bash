#feel free to add your commands here for easy access 
#if you don't want to scroll through past commands

#I'm sure you can put /bin/bash at the stop of this file and execute it to run multiple tests
#in sequence if that becomes necessary. Maybe not. 

#Pulse Secure -- name: SACVPN Server url: https://sacvpn.csus.edu 
#Use SacLink Credentials for Pulse Secure


#in mobaXterm follow steps
#Click on the "Session" button near the upper left corner.
#In the "Session settings" window that pops up, click on the "SSH" button near the upper left corner.
#Enter olympia.ecs.csus.edu (or whatever server you want) in the "Remote host" field. (NOTE: use the default port 22. The ports 50 to 54 were used with VNC and do not apply here)
#If you want you can also check "Specify username" and enter your username in the field to the right. Click OK.
#At the prompt, enter your username and password (or just password if you set the "Specify username" field).
#use credentials from Dr. Arad 


#should work for VCS Simulation Report but it stopped working for me for dve
vcs -PP buffer_fixture.v
simv 
dve

#Use this for dve if it stops working
#for dve: 
#click waveform button top of page
#click blue downward arrow to start simulation
#change timescale view sizing with white circle buttons with 2 and 1/2 inside circle
vcs -PP -debug_pp buffer_fixture.v
simv -gui


vcs -PP Program_Counter_fixture.v
vcs -PP -debug_pp Program_Counter_fixture.v

vcs -PP Instruction_Memory_fixture.v
vcs -PP -debug_pp Instruction_Memory_fixture.v

