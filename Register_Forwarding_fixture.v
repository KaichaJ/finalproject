`timescale 1ns / 1ps
`include "Register_Forwarding.v"

module Register_Forwarding_fixture;



reg [3:0] ID_EX_OP1, ID_EX_OP2, EX_MEM_OP1, MEM_WB_OP1; 
reg [1:0] EX_MEM_REGWRITE, MEM_WB_REGWRITE;
wire [1:0] FORWARDA, FORWARDB;
					
					
initial
        $vcdpluson;
		
initial
       $monitor($time, "  ForwardA = %b  ForwardB = %b\n                       ID_EX_Op1 = %b  EX_MEM_Op1 = %b  MEM_WB_Op1 = %b\n                       ID_EX_Op2 = %b  EX_MEM_Op1 = %b  MEM_WB_Op1 = %b\n                       EX_MEM_RegWrite = %b  MEM_WB_RegWrite = %b\n", FORWARDA[1:0], FORWARDB[1:0], ID_EX_OP1[3:0], EX_MEM_OP1[3:0], MEM_WB_OP1[3:0], ID_EX_OP2[3:0],EX_MEM_OP1[3:0],MEM_WB_OP1[3:0], EX_MEM_REGWRITE[1:0], MEM_WB_REGWRITE[1:0]);
	   
				
Register_Forwarding ex1(.ID_EX_Op1(ID_EX_OP1), .ID_EX_Op2(ID_EX_OP2), .EX_MEM_Op1(EX_MEM_OP1), 
						.MEM_WB_Op1(MEM_WB_OP1), .EX_MEM_RegWrite(EX_MEM_REGWRITE), 
						.MEM_WB_RegWrite(MEM_WB_REGWRITE), .ForwardA(FORWARDA), .ForwardB(FORWARDB));
					
					
//These inputs follow the register forwarding truth table exactly,
// and the monitor output will also follow the truth table in exact order
initial 
	begin
		ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b0010; MEM_WB_OP1 = 4'b0100; ID_EX_OP2 = 4'b1000; EX_MEM_REGWRITE = 2'b01; MEM_WB_REGWRITE = 2'b01;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b0010; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b0100; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b0010; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1000;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b0010; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1110; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1110;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b0100; ID_EX_OP2 = 4'b1000;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1110; ID_EX_OP2 = 4'b1110;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b0100; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1000;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b0100; ID_EX_OP2 = 4'b1000; EX_MEM_REGWRITE = 2'b00; MEM_WB_REGWRITE = 2'b01;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1000;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b0010; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1000; EX_MEM_REGWRITE = 2'b01; MEM_WB_REGWRITE = 2'b00;
		#10 ID_EX_OP1 = 4'b0001; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1000;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111;
		#10 ID_EX_OP1 = 4'b1111; EX_MEM_OP1 = 4'b1111; MEM_WB_OP1 = 4'b1111; ID_EX_OP2 = 4'b1111; EX_MEM_REGWRITE = 2'b00; MEM_WB_REGWRITE = 2'b00;
	end	




initial
	begin
       #300 $finish;
	end

endmodule
