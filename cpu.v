`include "Control_Unit.v"
`include "mux2to1.v"
`include "mux3to1.v"
`include "Data_Memory.v"
`include "Instruction_Memory.v"
`include "Program_Counter.v"
`include "buffer.v"
`include "hazardDetection.v"
`include "Register_Forwarding.v"
`include "ALUcontrol.v"
`include "ALU.v"
`include "adder.v"
`include "comparator.v"
`include "regFile.v"
`include "Extender.v"




module cpu(input clk, rst);
//unnamed wires CAPS
//Design Defined wires match design


//IF WIRES
wire [15:0] PC_TO_IM, PC_PLUS2, PC_PLUSOP, MUXTOPC, INSTRUCTION; 
wire IF_IDWrite, IF_IDHalt, PCWrite, addressSrc;

//IF regs
reg [31:0] IF_DATA; 
reg [15:0]OFFSET_2;




//ID WIRES
wire [15:0] RD1, RD2, R15_OUT, EXTENSION;
wire [12:0] CONTROL2MUX, EX_M_WB;
wire [12:0] ZERO = 0;
wire [3:0] R15_IN = 4'b1111;
wire [3:0] MUXTOREG;
wire [31:0] IF_IDout;
wire [1:0] ExtendType, BranchType;
wire jump; 
wire ID_Flush, Flush;

//ID regs
reg [68:0] ID_DATA;
reg OR_SELECT;

//EX wires
wire [31:0] ALUout;
wire [2:0] MUX2MEM;
wire [5:0] MUX2WB;			
wire [15:0] MUX2ALU1, MUX2ALU2, ALU_in1, ALU_in2;
wire [1:0] ForwardA, ForwardB;
wire [3:0] ALUCtrl;
wire branch;
wire [68:0] ID_EXout;


//EX regs
reg [80:0] EX_DATA;


//M wires
wire [15:0] MEMDATA_OUT;
wire [80:0] EX_Mout;
//M regs
reg [93:0] M_DATA;

//WB WIRES	
wire [93:0] M_WBout;
wire [15:0] MUX2WD1, MUX2WD2;


reg TIE_LOW = 1'b0;
reg TIE_HIGH = 1'b1;
wire EH;
reg HE;
reg [2:0] LOW1 = 0;
reg [5:0] LOW2 = 0;

//fetch
//always @(*)
//	$monitor($time, "  INSTRUCTION = %h  PC_TO_IM = %d  MUXTOPC = %d\n                      PC_PLUS2 = %d  OFFSET_2 = %d\n                      Instruction = %h  PC + 2 = %h\n                      IF_IDWrite = %b  IF_IDHalt = %b  rst = %b", INSTRUCTION, PC_TO_IM, MUXTOPC, PC_PLUS2, OFFSET_2, IF_IDout[31:16], IF_IDout[15:0], IF_IDWrite, IF_IDHalt, rst);

//always @(*) 
//	$monitor($time, "  ALUout = %h\n", ALUout);
	
	

	
always @(*)
	begin
		/*buffer inputs*/
		IF_DATA = {INSTRUCTION, PC_PLUS2}; 
		ID_DATA = {EXTENSION,RD2,RD1,IF_IDout[27:20],EX_M_WB}; 
		/*------------------------------------------Major Changes------------------------------------------------*/
		EX_DATA = {/*RD2*/ MUX2ALU2, /*RD1*/ MUX2ALU1, ALUout,ID_EXout[20:13],MUX2MEM,MUX2WB};
		/*-------------------------------------------------------------------------------------------------------*/
		M_DATA = {EX_Mout[80:65], EX_Mout[64:49], MEMDATA_OUT, EX_Mout[48:17], EX_Mout[16:9], EX_Mout[5:0]};
		//IF Stage
		OFFSET_2 = 2;
		
		//ID Stage
		OR_SELECT = ID_Flush | Flush;
		
		HE = EH;
		
	end 
	
	
/*permanent*/	
/*IF Stage*/

hazardDetection if1(.ID_EXMemRead(ID_EXout[7]), .jump(jump), .branch(branch), .ID_EXOp1(ID_EXout[20:17]), .IF_IDOp1(IF_IDout[27:24]), .IF_IDOp2(IF_IDout[23:20]), .PCwrite(PCWrite), .Flush(Flush), .IF_IDWrite(IF_IDWrite), .addressSrc(addressSrc)); 


mux2to1 #(.width(16)) if2(.a(PC_PLUS2), .b(PC_PLUSOP), 
						.sel(addressSrc), .out(MUXTOPC));

Program_Counter if3(.address_in(MUXTOPC), .clk(clk), .PCWrite(PCWrite), 
					.rst(rst), .address_out(PC_TO_IM));
					
adder if4(.data_in1(OFFSET_2), .data_in2(PC_TO_IM), .data_out(PC_PLUS2));

Instruction_Memory if5( .address_in(PC_TO_IM), .clk(clk), 
						.rst(rst), .address_out(INSTRUCTION));
						
							

buffer #(.REGSIZE(32)) ifid1(.data_in(IF_DATA), .clk(clk), 
							.w_enable1(IF_IDWrite), .w_enable2(IF_IDHalt), 
							.rst(rst), .data_out(IF_IDout));
							
/*permanent*/
//ID Stage
regFile id2(.RA1(IF_IDout[27:24]), .RA2(IF_IDout[23:20]), .WA1(M_WBout[13:10]), .WA2(MUXTOREG), .WD1(MUX2WD1), 
			.WD2(MUX2WD2), .w_enable(M_WBout[2:1]), .clk(clk), .reset(rst), 
			.RD1(RD1), .RD2(RD2), .R15(R15_OUT));
			
comparator id3(.branchType(BranchType), .RD1(RD1), .R15(R15_OUT), .branch(branch));

Control_Unit id4(.opcode(IF_IDout[31:28]), .functCode(IF_IDout[19:16]), .rst(rst), .ExtendType(ExtendType), .BranchType(BranchType), 
					.jump(jump), .ID_Flush(ID_Flush), .EX_Flush(EX_Flush), .IF_ID_Halt(IF_IDHalt),  
					.EX_M_WB(CONTROL2MUX));			
					
				//control mux							
mux2to1 #(.width(13)) id5(.a(CONTROL2MUX), .b(ZERO),     //done
						.sel(OR_SELECT), .out(EX_M_WB));
				//WA2 mux						
mux2to1 #(.width(4)) wb1(.a(M_WBout[9:6]), .b(R15_IN), 
						.sel(M_WBout[0]), .out(MUXTOREG));

Extender id6(.data_in(IF_IDout[27:16]), .ExtendType(ExtendType), .data_out(EXTENSION)); //done						
						
adder id7(.data_in1(IF_IDout[15:0]), .data_in2(EXTENSION), .data_out(PC_PLUSOP)); //done

buffer #(.REGSIZE(69)) idex1( .data_in(ID_DATA), .clk(clk), 
							.w_enable1(TIE_HIGH), .w_enable2(TIE_LOW), //maybe not allowed
							.rst(rst), .data_out(ID_EXout));							
//EX Stage


//memory flush mux
mux2to1 #(.width(3)) ex1(.a(ID_EXout[8:6]), .b(LOW1), .sel(EX_Flush), .out(MUX2MEM));   //not allowed?

//wb flush mux
mux2to1 #(.width(6)) ex2(.a(ID_EXout[5:0]), .b(LOW2), .sel(EX_Flush), .out(MUX2WB));	//not allowed?

//ALUSrc1 mux																	
mux2to1 #(.width(16)) ex3(.a(MUX2ALU1), .b(ID_EXout[68:53]), .sel(ID_EXout[9]), .out(ALU_in1));		//done

//ALUSrc2 mux																	
mux2to1 #(.width(16)) ex4(.a(MUX2ALU2), .b(ID_EXout[68:53]), .sel(ID_EXout[10]), .out(ALU_in2));		//done	
			
//ForwardA mux															
mux3to1 #(.width(16)) ex5(.a(ID_EXout[36:21]), .b(EX_Mout[32:17]), .c(MUX2WD1), .sel(ForwardA), .out(MUX2ALU1));   //done

//ForwardB mux															
mux3to1 #(.width(16)) ex6(.a(ID_EXout[52:37]), .b(EX_Mout[32:17]), .c(MUX2WD1), .sel(ForwardB), .out(MUX2ALU2));   //done

ALU ex7(.op1(ALU_in1), .op2(ALU_in2), .ALUCtrl(ALUCtrl), .overflow(EH), .data_out(ALUout));

ALUcontrol ex8(.aluOp(ID_EXout[12:11]), .functCode(ID_EXout[56:53]), .rst(rst), .ctrl(ALUCtrl));    //done

Register_Forwarding ex9(.ID_EX_Op1(ID_EXout[20:17]), .ID_EX_Op2(ID_EXout[16:13]), .EX_MEM_Op1(EX_Mout[16:13]), .MEM_WB_Op1(M_WBout[13:10]), .EX_MEM_RegWrite(EX_Mout[2:1]), .MEM_WB_RegWrite(M_WBout[2:1]), .ForwardA(ForwardA), .ForwardB(ForwardB)); //done


/*------------------------------------------Major Changes------------------------------------------------*/
buffer #(.REGSIZE(81)) exm1( .data_in(EX_DATA), .clk(clk), 
							.w_enable1(TIE_HIGH), .w_enable2(TIE_LOW), //maybe not allowed
							.rst(rst), .data_out(EX_Mout));




//M Stage                                     //FIXED change occurred here
Data_Memory m1(.address(EX_Mout[24:17]), .data_in(EX_Mout[64:49]), 
					.clk(clk), .rst(rst), .w_enable(EX_Mout[8]), .r_enable(EX_Mout[7]), .byte_enable(EX_Mout[6]), .data_out(MEMDATA_OUT));
/*------------------------------------------------------------------------------------------*/
buffer #(.REGSIZE(94)) mwb1( .data_in(M_DATA), .clk(clk), 
							.w_enable1(TIE_HIGH), .w_enable2(TIE_LOW), //maybe not allowed
							.rst(rst), .data_out(M_WBout));

//WB Stage

mux3to1 #(.width(16)) wb2(.a(M_WBout[29:14]), .b(M_WBout[61:46]), .c(M_WBout[93:78]), .sel(M_WBout[4:3]), .out(MUX2WD1));   //done

mux2to1 #(.width(16)) wb3(.a(M_WBout[45:30]), .b(M_WBout[77:62]), .sel(M_WBout[5]), .out(MUX2WD2));		//done	

endmodule