`include "Control_Unit.v"
`include "mux2to1.v"
`include "adder.v"
`include "comparator.v"
`include "regFile.v"
`include "Extender.v"

//unnamed wires CAPS
//Design Defined wires match design

module fetch(/*permanent*/input clk, rst);



//IF WIRES
wire [15:0] PC_TO_IM, PC_PLUS2, MUXTOPC, INSTRUCTION; 
wire IF_IDWrite, IF_IDHalt, PCWrite, addressSrc;

//IF regs
reg [31:0] IF_DATA; 
reg [15:0]OFFSET_2, PC_PLUSOP;




//ID WIRES
wire [15:0] RD1, RD2, R15_OUT, EXTENSION;
wire [12:0] CONTROL2MUX, EX_M_WB;
wire [12:0] ZERO := 0;
wire [3:0] R15_IN := 4'b1111;
wire [3:0] MUXTOREG;
wire [31:0] IF_IDout;
wire [1:0] ExtendType, BranchType;


//ID regs
reg [68:0] ID_DATA;
reg OR_SELECT, ID_Flush, Flush;

always @(*)
	$monitor($time, "  INSTRUCTION = %h  PC_TO_IM = %d  MUXTOPC = %d\n                      PC_PLUS2 = %d  OFFSET_2 = %d\n                      Instruction = %h  PC + 2 = %h\n                      IF_IDWrite = %b  IF_IDWrite = %b  rst = %b", INSTRUCTION, PC_TO_IM, MUXTOPC, PC_PLUS2, OFFSET_2, IF_IDout[31:16], IF_IDout[15:0], IF_IDWrite, IF_IDHalt, rst);


//WB WIRES	
wire [93:0] M_WBout;
	
always @(*)
	begin
		/*buffer inputs*/
		IF_DATA = {INSTRUCTION, PC_PLUS2}; 
		ID_DATA = {EXTENSION,RD2,RD1,IF_IDout[27:20],EX_M_WB};
		
		//IF Stage
		OFFSET_2 = 2;
		
		//ID Stage
		OR_SELECT = ID_Flush | Flush;
		
		
	end 
	
	
/*permanent*/	
/*IF Stage*/
mux2to1 #(.width(16)) if1(.a(PC_PLUS2), .b(PC_PLUSOP), 
						.sel(addressSrc), .out(MUXTOPC));

Program_Counter if2(.address_in(MUXTOPC), .clk(clk), .PCWrite(PCWrite), 
					.rst(rst), .address_out(PC_TO_IM));
					
adder if3(.data_in1(OFFSET_2), .data_in2(PC_TO_IM), .data_out(PC_PLUS2));

Instruction_Memory if4( .address_in(PC_TO_IM), .clk(clk), 
						.rst(rst), .address_out(INSTRUCTION));
						
							

buffer #(.REGSIZE(32)) ifid1(.data_in(IF_DATA), .clk(clk), 
							.w_enable1(IF_IDWrite), .w_enable2(IF_IDHalt), 
							.rst(rst), .data_out(IF_IDout));
							
/*permanent*/
//ID Stage
regFile id2(.RA1(IF_IDout[27:24]), .RA2(IF_IDout[23:20]), .WA1(M_WBout[:]), .WA2(MUXTOREG), .WD1(), 
			.WD2(), .w_enable(M_WBout[:]), .clk(clk), .reset(rst), 
			.RD1(RD1), .RD2(RD2), .R15(R15_OUT));
			
comparator id3(.branchType(BranchType), .RD1(RD1), .R15(R15_OUT), .branch(/*HazardDetect*/));

Control_Unit id4(.opcode(IF_IDout[31:28]), .functCode(IF_IDout[19:16]), .rst(rst), .ExtendType(ExtendType), .BranchType(BranchType), 
					.jump(/*hazarddetection*/), .ID_Flush(ID_Flush), .EX_Flush(/*toEX*/), .IF_ID_Halt(/*hazarddetection*/),  
					.EX_M_WB(CONTROL2MUX));			
					
				//control mux							
mux2to1 #(.width(13)) id5(.a(CONTROL2MUX), .b(ZERO),     //done
						.sel(OR_SELECT), .out(EX_M_WB));
				//WA2 mux						
mux2to1 #(.width(4)) wb1(.a(M_WBout[:]), .b(R15_IN), 
						.sel(addressSrc), .out(MUXTOREG));

Extender id6(.data_in(IF_IDout[27:16]), .ExtendType(ExtendType), .data_out(EXTENSION)); //done						
						
adder id7(.data_in1(IF_IDout[15:0]), .data_in2(EXTENSION), .data_out(PC_PLUSOP)); //done

buffer #(.REGSIZE(69)) idex1( .data_in(ID_DATA), .clk(clk), 
							.w_enable1(1), .w_enable2(1), //maybe not allowed
							.rst(rst), .data_out(ID_EXout));							
//EX Stage






//M Stage

//WB Stage
