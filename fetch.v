`include "Program_Counter.v"
`include "adder.v"
`include "Instruction_Memory.v"
`include "buffer.v"
`include "mux2to1.v"

//unnamed wires CAPS
//Design Defined wires match design

module fetch(/*permanent*/input clk, rst,
			/*temporary*/input IF_IDWrite, IF_IDHalt, PCWrite, addressSrc, 
						output [31:0] IF_IDout);

/*permanent*/
wire [15:0] PC_TO_IM, PC_PLUS2, MUXTOPC, INSTRUCTION;
reg [31:0] IF_DATA; 
reg [15:0]OFFSET_2, PC_PLUSOP;
//wire IF_IDWrite, IF_IDHalt, PCWrite, addressSrc;

/*temporary*/
wire [31:0] IF_IDout;
//IF/ID
always @(*)
	$monitor($time, "  INSTRUCTION = %h  PC_TO_IM = %d  MUXTOPC = %d\n                      PC_PLUS2 = %d  OFFSET_2 = %d\n                      Instruction = %h  PC + 2 = %h\n                      IF_IDWrite = %b  IF_IDWrite = %b  rst = %b", INSTRUCTION, PC_TO_IM, MUXTOPC, PC_PLUS2, OFFSET_2, IF_IDout[31:16], IF_IDout[15:0], IF_IDWrite, IF_IDHalt, rst);


always @(*)
	begin
		/*permanent*/
		IF_DATA = {INSTRUCTION, PC_PLUS2};
		OFFSET_2 = 2;
		
		/*temporary*/ 
		PC_PLUSOP = 54;
	end 
	
	
/*permanent*/	
/*IF Stage*/
mux2to1 #(.width(16)) if1(.a(PC_PLUS2), .b(PC_PLUSOP), 
						.sel(addressSrc), .out(MUXTOPC));

Program_Counter if2(.address_in(MUXTOPC), .clk(clk), .PCWrite(PCWrite), 
					.rst(rst), .address_out(PC_TO_IM));
					
adder if3(.data_in1(OFFSET_2), .data_in2(PC_TO_IM), .data_out(PC_PLUS2));

Instruction_Memory if4( .address_in(PC_TO_IM), .clk(clk), 
						.rst(rst), .address_out(INSTRUCTION));

buffer #(.REGSIZE(32)) if5( .data_in(IF_DATA), .clk(clk), 
							.w_enable1(IF_IDWrite), .w_enable2(IF_IDHalt), 
							.rst(rst), .data_out(IF_IDout));
							


endmodule


//ID Stage

//EX Stage

//M Stage

//WB Stage
