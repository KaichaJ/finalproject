`include "hazardDetection.v"
module hazardDetection_fixture;
reg ID_EXMemRead, jump, branch;
reg [3:0] ID_EXOp1, IF_IDOp1, IF_IDOp2;
wire PCwrite, Flush, IF_IDWrite, addressSrc; 

initial 
	$vcdpluson;

initial
	$monitor($time, "\n ID_EXMemRead = %b jump = %b branch = %b \n ID_EXOp1 = %d IF_IDOp1 = %d IF_IDOp2 = %d \n PCwrite = %b Flush = %b IF_IDWrite = %b addressSrc = %b", ID_EXMemRead, jump, branch, ID_EXOp1 [3:0], IF_IDOp1 [3:0], IF_IDOp2 [3:0], PCwrite, Flush, IF_IDWrite, addressSrc);

hazardDetection h1 (.ID_EXMemRead(ID_EXMemRead), .jump(jump), .branch(branch), .ID_EXOp1(ID_EXOp1), .IF_IDOp1(IF_IDOp1), .IF_IDOp2(IF_IDOp2), .PCwrite(PCwrite), .Flush(Flush), .IF_IDWrite(IF_IDWrite), .addressSrc(addressSrc));

initial
begin	
	//No match with MEMREAD high  
	ID_EXMemRead = 1;
	jump = 0; 
	branch = 0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	#10 
	//match with MEMREAD high EX OP1 ID OP1  
	ID_EXMemRead = 1;
	jump = 0; 
	branch = 0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 5;
	IF_IDOp2 = 3;
	#10
	//do nothing
	ID_EXMemRead = 0;
	jump = 0; 
	branch =0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	#10    
	//match with MEMREAD high EX OP1 ID OP2  
	ID_EXMemRead = 1;
	jump = 0; 
	branch = 0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 5;
	#10 
	//do nothing
	ID_EXMemRead = 0;
	jump = 0; 
	branch =0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	#10 
	//branch high
	ID_EXMemRead = 0;
	jump = 0; 
	branch =1;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	#10
	//do nothing
	ID_EXMemRead = 0;
	jump = 0; 
	branch =0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	#10
	//jump high
	ID_EXMemRead = 0;
	jump = 1; 
	branch =0;
	ID_EXOp1 = 5;
	IF_IDOp1 = 7;
	IF_IDOp2 = 3;
	      
end

initial
	#100 $finish;

endmodule                         
