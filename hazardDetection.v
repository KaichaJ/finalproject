module hazardDetection(input ID_EXMemRead, jump, branch, input [3:0] ID_EXOp1, IF_IDOp1, IF_IDOp2, output reg PCwrite, Flush, IF_IDWrite, addressSrc); 

always@(*) 
begin
if(ID_EXMemRead)
begin
	if(ID_EXOp1 == IF_IDOp2 || ID_EXOp1 == IF_IDOp1)
	begin
	PCwrite = 0;
	Flush = 1;
	IF_IDWrite = 0; 
	addressSrc = 0 ;
	end
	else 
	begin
	PCwrite = 1;
	Flush = 0;
	IF_IDWrite = 1; 
	addressSrc = 0 ;
	end
	
end

else if(jump || branch)
begin
	PCwrite = 1;
	Flush = 1;
	IF_IDWrite = 1; 
	addressSrc =1 ;
end

else 
begin
	PCwrite = 1;
	Flush = 0;
	IF_IDWrite = 1; 
	addressSrc = 0 ;
end

end
endmodule
