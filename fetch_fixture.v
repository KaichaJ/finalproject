`timescale 1ns / 1ps 
`include "fetch.v"

module fetch_fixture;




/*permanent*/
reg CLK, RST;
/*temporary*/
reg IF_IDWRITE, IF_IDHALT, PCWRITE, ADDRESSSRC; 
wire [31:0] IF_IDOUT;


initial $vcdpluson;


/*temporary
//IF/ID
initial
       $monitor($time, "  Instruction = %h  PC + 2 = %h\n                      W_ENABLE1 = %b  W_ENABLE2 = %b  RST = %b", DATA_OUT[31:16], DATA_OUT[15:0], W_ENABLE1, W_ENABLE2, RST);
*/


fetch if1(/*permanent*/.clk(CLK), .rst(RST),
		/*temporary*/.IF_IDWrite(IF_IDWRITE), .IF_IDHalt(IF_IDHALT), .PCWrite(PCWRITE), .addressSrc(ADDRESSSRC), 
					.IF_IDout(IF_IDOUT));
						
						
						
initial
	begin
		CLK = 1'b0;
		forever #5 CLK = ~CLK;
	end


initial 
	begin
		RST = 1'b0;
		#5 RST = 1'b1;
	end


initial 
	begin
		IF_IDWRITE = 1'b1; IF_IDHALT = 1'b1; PCWRITE = 1'b1;
		//#40 IF_IDWRITE = 1'b0; IF_IDHALT = 1'b1; PCWRITE = 1'b0; // testing with stall
		//#10 IF_IDWRITE = 1'b1; IF_IDHALT = 1'b1; PCWRITE = 1'b1;
		//#10 IF_IDWRITE = 1'b1; IF_IDHALT = 1'b0; PCWRITE = 1'b0; // testing with stall
		//#10 IF_IDWRITE = 1'b1; IF_IDHALT = 1'b1; PCWRITE = 1'b1;
	end	

	

initial 
	begin
		ADDRESSSRC = 1'b0;  
		//#15 ADDRESSSRC = 1'b1; //testing with jump or branch
	end


initial
	begin
       #800 $finish;
	end

endmodule